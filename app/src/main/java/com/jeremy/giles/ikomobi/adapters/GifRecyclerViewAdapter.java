package com.jeremy.giles.ikomobi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jeremy.giles.ikomobi.R;

public class GifRecyclerViewAdapter extends RecyclerView.Adapter<GifRecyclerViewAdapter.ViewHolder>{

    private String[] gifs = new String[0];
    private Context context;

    public GifRecyclerViewAdapter(Context context, String[] gifs) {
        this.context = context;
        this.gifs = gifs;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_gif_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String url = gifs[position];
        Glide.with(context).load(url).into(holder.gif);
    }

    @Override
    public int getItemCount() {
        return gifs.length;
    }

    public String getItem(int position) {
        return gifs[position];
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView gif;

        public ViewHolder(View itemView) {
            super(itemView);
            gif = (ImageView) itemView.findViewById(R.id.gif);
        }
    }
}
