package com.jeremy.giles.ikomobi.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jeremy.giles.ikomobi.adapters.GifRecyclerViewAdapter;
import com.jeremy.giles.ikomobi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "MainActivity";

    // TODO Change PUBLIC_BETA_KEY to Product KEY
    private static final String PUBLIC_BETA_KEY = "3d4f1971da184689827d7a1ae6264360";
    private static final String BASE_URL = "https://api.giphy.com/";
    private static final String TRENDING_PATH = "v1/gifs/trending";
    private static final int LIMIT = 20;
    private static final String RATING = "G";

    private RequestQueue queue;
    private static String urlProvider() {
        return BASE_URL + TRENDING_PATH + "?api_key=" + PUBLIC_BETA_KEY + "&limit=" + LIMIT + "&rating=" + RATING;
    }

    private SwipeRefreshLayout swipeRefreshGifs;
    private RecyclerView recyclerView;
    private GifRecyclerViewAdapter adapter;
    private String[] a_gifs = new String[LIMIT];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this);

        recyclerView = (RecyclerView) findViewById(R.id.rv_gifs);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        adapter = new GifRecyclerViewAdapter(this, a_gifs);
        recyclerView.setAdapter(adapter);
        makeJsonArrayRequest();

        swipeRefreshGifs = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshGifs);
        swipeRefreshGifs.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                       makeJsonArrayRequest();
                    }
                }
        );

    }

    private void makeJsonArrayRequest() {

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                urlProvider(), null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                        try {
                            JSONArray data = (JSONArray) response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {

                                JSONObject gifs = (JSONObject) data.get(i);
                                JSONObject images = gifs.getJSONObject("images");
                                JSONObject gif = images.getJSONObject("downsized");
                                String url = gif.getString("url");
                                a_gifs[i] = url;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        recyclerView.setAdapter(new GifRecyclerViewAdapter(getApplicationContext(), a_gifs));
                        adapter.notifyDataSetChanged();
                        swipeRefreshGifs.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(LOG_TAG, "Error: " + error.getMessage());
            }
        });

        queue.add(req);
    }
}
